<?php
    function CreateForm($num, $value) {
        $questions = array(
            1=>'Can you swim?', 
            2=>'Did he go to work or to school?', 
            3=>'Has your class finished?', 
            4=> 'Where is my pen?',
            5=>'Who did you visit?', 
            6=>'Shall we go to your place or mine?', 
            7=>'When will Lucy arrive?', 
            8=>'Who called here so late?',
            9=>'Do you want to watch a movie?',
            10=>'Have you done the laundry?'
        );

        $results = array(
            1 =>array(
                array('text'=>'In a pool', 'bool' => 0),
                array('text'=>'Yes, I can', 'bool' => 1),
                array('text'=>'Very good', 'bool' => 0)),
           
            2 =>array(
                array('text'=>'To work', 'bool' => 1),
                array('text'=>"No, he doesn't", 'bool' => 0),
                array('text'=>'At 3:00 PM', 'bool' => 0)),
            3 =>array(
                array('text'=>'Yes, it has', 'bool' => 1),
                array('text'=>'In five minutes', 'bool' => 0),
                array('text'=>"It's English", 'bool' => 0)),
            4 =>array(
                array('text'=>"Because it's lost", 'bool' => 0),
                array('text'=>'On the table', 'bool' => 1),
                array('text'=>"No, you didn't", 'bool' => 0)),
            5 =>array(
                array('text'=>'I visit my mother', 'bool' => 0),
                array('text'=>'Yes, I did', 'bool' => 0),
                array('text'=>'I visited Judy', 'bool' => 1)),
            6 =>array(
                array('text'=>'My place', 'bool' => 1),
                array('text'=>'It is yours
                ', 'bool' => 0),
                array('text'=>'Yes, we shall', 'bool' = >0)),
            7 =>array(
                array('text'=>'At 7 PM', 'bool' => 1),
                array('text'=>"No, she won't", 'bool' => 0),
                array('text'=>'From France', 'bool' => 0)),
            8 =>array(
                array('text'=>"It's midnight", 'bool' => 0),
                array('text'=>'It was Ryan', 'bool' => 1),
                array('text'=>'Yes, I called', 'bool' => 0)),
            9 =>array(
                array('text'=>"At the cinemat", 'bool' => 0),
                array('text'=>'Yes, I watched it', 'bool' => 0),
                array('text'=>"No, I don't", 'bool' => 1)),
            10 =>array(
                array('text'=>"Yes, I do", 'bool'=>0),
                array('text'=>'On Wednesdays', 'bool'=>0),
                array('text'=>"No, I haven't", 'bool'=>1)),
            
        );
        // Khởi tạo mảng lưu đáp án đúng
        $trueAnswers = [];
        // Khởi tạo mảng lưu câu trả lời
        $selectAnswers = [];
        // Chỉ hiện thị 5 câu hỏi trong mỗi trang
        $arr = (array_chunk($questions, 5, true));
        $selectQuestions = $arr[$num];

        echo '<div class="form__list">';
        
        foreach($selectQuestions as $keyQuestion=>$question)
        {
            // Lấy đáp án được chọn
            if (isset($_POST[$keyQuestion])){
                $selectAnswers[$keyQuestion] = $_POST[$keyQuestion];
            }
            // Hiển thị câu hỏi
            echo '
            <div class="form__item">
                <div class="form__question">
                    <h3 class="form__question-num">Câu '.$keyQuestion.':</h3>
                    <p class="form__question-text">'.$question.'</p>
                </div>
                <div class="form__answer">';
                foreach($results as $keyResult=>$result) {
                    if ($keyQuestion==$keyResult){
                        for($i=0;$i<count($result);$i++){
                            $answers = $result[$i];
                            if ($answers['bool']==1){
                                $trueAnswers[$keyQuestion]=$answers['text'];
                            }
                            $answer = $answers['text'];
                            if ($answer) {
                                echo '
                                <div class="form__answer-item">
                                    <input type="radio" class="form__answer-select" name="'.$keyQuestion.'" value="'.$answer.'"/>
                                    <span class="form__answer-text">'.$answer.'</span>
                                    </div> 
                                ';
                            }
                        }
                    }
                }
                echo'      
                </div> 
            </div>';
        }
        echo '
        </div>
        <div class="form__button">
            <input type="submit" class="form__next" value="'.$value.'" name="submit"/>
        </div>
        
        ';
        if (isset($_POST['submit'])) {
            foreach($selectAnswers as $key=>$answer) {
                if ($answer == $trueAnswers[$key]){
                    $count++;
                }
            }
            return $count;
        }
    }
?>
