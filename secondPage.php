<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Page 2</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="main.css" />
    </head>
    <body>
        <form method="POST" action="">
            <div class="form">
                <h1 class="form__title">Quiz 2</h1>
                <?php
                    if (isset($_POST['submit'])) {
                        header('Location: result.php');
                    }
                    include('createForm.php');
                    $_SESSION['score'] = CreateForm(1, 'Submit') + $_SESSION['first'];
                ?>
            </div>
        </form>
    </body>
</html>
