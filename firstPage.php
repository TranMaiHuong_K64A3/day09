<!DOCTYPE html>
<?php
// setcookie('first', '0', time() + 3600,);
session_start();
?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Page 1</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="main.css" />
    </head>
    <body>
        <form method="POST" action="">
            <div class="form">
                <h1 class="form__title">Quiz 1</h1>
                <?php

                    if (isset($_POST['submit'])) {
                        header('Location: secondPage.php');
                    }
                    include('createForm.php');
                    $_SESSION['first'] = CreateForm(0, 'Next');
                ?>
            </div>
           
        </form>
    </body>
</html>
